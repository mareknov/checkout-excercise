/**
  * Created by mareknov on 09/03/16.
  */

import scala.collection.immutable._

object Checkout{

  def sumTotal(list: List[Fruit]) : BigDecimal = {
    val total = list.foldLeft(BigDecimal(0))((sum, fruit) => sum + fruit.price)
    return total
  }

  def sumTotalWithOffers(list: List[Fruit]) : BigDecimal = {
    sumTotal(applyOffer(list))
  }

  def fromStrings(list: List[String]) : List[Fruit] = {
    val fruits = list.map(s => {
      s.toLowerCase() match{
        case (Apple.name) => Some(Apple)
        case (Orange.name) => Some(Orange)
        case default => None
      }
    })
    return fruits.flatten
  }

  def fromPrices(list: List[Double]) : List[Fruit] = {
    val fruits = list.map(price => {
      BigDecimal(price) match{
        case (Apple.price) => Some(Apple)
        case (Orange.price) => Some(Orange)
        case default => None
      }
    })
    return fruits.flatten
  }

  def applyOffer(list: List[Fruit]) : List[Fruit] = {
    val fruitMap = list.foldLeft(new HashMap[Fruit, Int])((map, entry) => {
      entry match{
        case (Apple) => map + (Apple -> (map.getOrElse(Apple, 0) + 1))
        case (Orange) => map + (Orange -> (map.getOrElse(Orange, 0) + 1))
      }
    })

    val countA = fruitMap.getOrElse(Apple, 0)
    val countO = fruitMap.getOrElse(Orange, 0)
    val apples = listFruits((countA / 2) + (countA % 2), Apple)
    val oranges = listFruits((countO / 3) * 2 + (countO % 3), Orange)
    apples ++ oranges
  }

  def listFruits(num: Int, fruit: Fruit) : List[Fruit] = new Range(0, num, 1).map(_ => fruit).toList
}
