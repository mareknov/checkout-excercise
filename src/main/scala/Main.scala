/**
  * Created by mareknov on 09/03/16.
  */
object Main extends App{

  val listFruits = List(Apple, Apple, Orange, Apple, Orange, Orange, Orange, Apple, Orange)
  val listStrings = List("apple", "ApplE", "ALSKsdk", "ORANGE", "apple")
  val listPrices = List(0.60, 0.60, 0.87, 1.245, 0.25, 0.60)

  println("Total price fruits: " +  Checkout.sumTotal(listFruits))
  println("Total price strings: " +  Checkout.sumTotal(Checkout.fromStrings(listStrings)))
  println("Total price prices: " +  Checkout.sumTotal(Checkout.fromPrices(listPrices)))
  println()

  // Example: after applying offers
  // 4 Apples => 2 Apples
  // 5 Oranges => 4 Oranges
  println(Checkout.applyOffer(listFruits))
  println("Total price + offers: " +  Checkout.sumTotalWithOffers(listFruits))
}
