/**
  * Created by mareknov on 09/03/16.
  */

sealed abstract class Fruit(p: BigDecimal, n: String){
  val price = p
  val name = n
}

case object Apple extends Fruit(0.60, "apple")

case object Orange extends Fruit(0.25, "orange")
